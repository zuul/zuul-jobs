Container Jobs
==============

.. zuul:autojob:: build-container-image
.. zuul:autojob:: upload-container-image
.. zuul:autojob:: promote-container-image
.. zuul:autojob:: mirror-container-images

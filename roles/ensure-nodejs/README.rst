Install NodeJS from nodesource

**Role Variables**

.. zuul:rolevar:: node_version

   Required. What version of Node to install.
